package az.ingress.HelloAppDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloAppDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloAppDemoApplication.class, args);
	}

}
